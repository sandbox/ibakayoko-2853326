This module allows to send <a href="https://www.drupal.org/project/webform">Webform </a>submissions to <a href="https://www.icontact.com/">iContact</a> list.

<strong>Requirements</strong>
<ul>
  <li>Webform module (https://www.drupal.org/project/webform).</li>
  <li>iContact Account and App
  <ul>
       <li>Signup for iContact Account (https://www.icontact.com/signup)</li>
       <li>Click on "Settings and Billing" and "iContact API" to create Your application</>
    </ul>
  </li>
</ul>

<strong>Installation</strong>
<ol>
  <li>Unzip the files to the "modules" OR "modules/contrib" directory and enable the module.</li>
  <li>Go to IContact Configuration page  (admin/config/services/icontact)  to provide iContact API and Account ID info</li>
  <li>Go to Webforms list page (admin/structure/webform) and click "Edit" on desired Webform.</li>
  <li>Click Emails/Handlers secondary tab and then click on "Add handler" button.</li>
  <li>Click on "Add handler" button on "iContact" row.</li>
  <li>Fill in the form. You should have at least one Folder and List in your iContact account, and at least one Email field in your Webform.</li>
</ol>

If you want to map extra fields, create a sign-up form at your iContact account. Add as many form items as you want, but take into account "Field Name" of each one. Each field you have configured in your Webform, will be mapped there if the "key" value in the Webform matches "Field tag" value in iContact. 

iContact default fields https://www.icontact.com/developerportal/documentation/contacts