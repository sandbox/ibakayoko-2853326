<?php
namespace Drupal\webform_icontact\Plugin\WebformHandler;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form submission to MailChimp handler.
 *
 * @WebformHandler(
 * id = "icontact",
 * label = @Translation("iContact"),
 * category = @Translation("iContact"),
 * description = @Translation("Sends a form submission to a iContact list."),
 * cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 * results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class WebformIcontactHandler extends WebformHandlerBase
{

    /**
     *
     * {@inheritdoc}
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, WebformSubmissionConditionsValidatorInterface $conditions_validator)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $logger_factory, $config_factory, $entity_type_manager, $conditions_validator);
        $this->setConfiguration($configuration);
        $this->loggerFactory = $logger_factory;
        $this->configFactory = $config_factory;
        $this->submissionStorage = $entity_type_manager->getStorage('webform_submission');
        $this->conditionsValidator = $conditions_validator;
    }

    /**
     *
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static($configuration, $plugin_id, $plugin_definition, $container->get('logger.factory'), $container->get('config.factory'), $container->get('entity_type.manager'), $container->get('webform_submission.conditions_validator'));
    }

    /**
     *
     * {@inheritdoc}
     *
     */
    public function getSummary()
    {
        $folderId = $this->configuration['folder'];
        return [
            // '#theme' => 'markup',
            '#markup' => '<strong>' . $this->t('Folder ID') . ': </strong>' . ($folderId) . '<br/><strong>' . $this->t('List Name') . ': </strong>' . ($this->getListName($this->configuration['list'], $folderId))
        ];
    }

    /**
     *
     * {@inheritdoc}
     *
     */
    public function defaultConfiguration()
    {
        return [
            'folder' => '',
            'list' => '',
            'field_mapping' => ''
        ];
    }

    /**
     *
     * {@inheritdoc}
     *
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {
        $folders = webform_icontact_get_client_folders();
        
        $default_option = array();
        
        $default_option['0'] = $this->t('- Select an option -');
        $clientFolders = $default_option + $folders;
        
        $selectedFolder = (! empty($form_state->getValue(array(
            'settings',
            'folder'
        )))) ? $form_state->getValue(array(
            'settings',
            'folder'
        )) : $this->configuration['folder'];
        
        $form['folder'] = [
            '#type' => 'select',
            '#title' => $this->t('Folder'),
            '#required' => TRUE,
            '#default_value' => $selectedFolder,
            '#options' => $clientFolders,
            '#ajax' => array(
                'callback' => [
                    $this,
                    'changeOptionsAjax'
                ],
                'wrapper' => 'dropdown_second_replace'
            )
        ];
        
        $selectedList = (empty($form_state->getValue(array(
            'settings',
            'list'
        )))) ? $this->configuration['list'] : '0';
        
        $form['list'] = array(
            '#type' => 'select',
            '#required' => TRUE,
            '#title' => $this->t('List'),
            '#options' => $default_option + webform_icontact_get_client_folder_lists($selectedFolder),
            '#default_value' => $selectedList,
            '#prefix' => '<div id="dropdown_second_replace">',
            '#suffix' => '</div>',
            '#validated' => TRUE
        );
        
        $form['field_mapping'] = array(
            '#type' => 'details',
            '#title' => $this->t('iContact Attribute Mappings'),
            '#description' => $this->t('Mapping of the webform field with iContact fields. Entries left blank will be ignored.'),
            '#tree' => TRUE,
            '#open' => TRUE
        );
        
        $fields = $this->getWebform()->getElementsDecoded();
        
        foreach ($fields as $field_name => $field) {
            
            if ($field['#type'] != 'webform_actions') {
                $form['field_mapping'][$field_name] = [
                    '#type' => 'textfield',
                    '#title' => $field_name,
                    '#default_value' => (! empty($this->configuration['field_mapping'][$field_name])) ? $this->configuration['field_mapping'][$field_name] : ' '
                ];
                
                if ($field['#type'] == 'email' || ! empty($field['#required'])) {
                    $form['field_mapping'][$field_name]['#required'] = TRUE;
                }
            }
        }
        
        return $form;
    }

    /**
     *
     * {@inheritdoc}
     *
     */
    public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitConfigurationForm($form, $form_state);
        $values = $form_state->getValues();
        
        foreach ($this->configuration as $name => $value) {
            if (isset($values[$name])) {
                $this->configuration[$name] = $values[$name];
            }
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     */
    public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE)
    {
        if (! $update) {
            $fields = $webform_submission->toArray(TRUE);
            $sendFields = array();
            foreach ($fields['data'] as $field_name => $field) {
                $sendFields[$this->configuration['field_mapping'][$field_name]] = $field;
            }
            
            $contactResult = webform_icontact_post_contacts($this->configuration['folder'], $this->configuration['list'], $sendFields);
            
            if ($contactResult) {
                // We check if the contact has already subscribed to the list
                $subscribed = webform_icontact_check_subscribe_to_list($contactResult['contactId'], $contactResult['listId'], $contactResult['folderId']);
                
                if (is_bool($subscribed)) {
                    $subscriptionResult = webform_icontact_subscribe_to_list($contactResult);
                }
            }
            
            if (! $contactResult) {
                // Log error message.
                $context = [
                    '@form' => $this->getWebform()->label(),
                    '@list' => $this->configuration['list']
                ];
                $this->loggerFactory->get('webform_icontact')->error('@form webform failed to post contact to iContact.', $context);
            } else {
                if (empty($subscriptionResult) && is_bool($subscribed)) {
                    // Log error message.
                    $context = [
                        '@form' => $this->getWebform()->label(),
                        '@list' => $this->configuration['list'],
                        '@subscribe' => $subscribed
                    ];
                    $this->loggerFactory->get('webform_icontact')->error('@form webform failed to subscribe @subscribe the contact to iContact list @list .', $context);
                }
            }
        }
    }

    /**
     * Ajax callback to validate the email field.
     */
    public function changeOptionsAjax(array &$form, FormStateInterface $form_state)
    {
        // $this->loggerFactory->get('webform_icontact')->notice('<pre><code>' . print_r($form['settings']['list'], TRUE) . '</code></pre>');
        $selectFolder = $form_state->getValue(array(
            'settings',
            'folder'
        ));
        
        $default_option = array(
            '0' => $this->t('- Select an option -')
        );
        
        $form['settings']['list']['#options'] = (! empty($selectFolder)) ? $default_option + webform_icontact_get_client_folder_lists($form_state->getValue(array(
            'settings',
            'folder'
        ))) : $default_option;
        
        return $form['settings']['list'];
    }

    /*
     * Get list name
     */
    private function getListName($listId, $folderId)
    {
        $list = webform_icontact_get_list($listId, $folderId);
        return $list['name'];
    }
}
