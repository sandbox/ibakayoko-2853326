<?php
namespace Drupal\webform_icontact\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure iContact API settings for this site.
 */
class SettingsForm extends ConfigFormBase
{

    /**
     *
     * {@inheritdoc}
     */
    public function getFormID()
    {
        return 'webform_icontact_admin_settings';
    }

    protected function getEditableConfigNames()
    {
        return [
            'webform_icontact.settings'
        ];
    }

    /**
     *
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('webform_icontact.settings');
        
        $form['icontact_api_url'] = array(
            '#type' => 'textfield',
            '#title' => t('Api URL'),
            '#required' => TRUE,
            '#default_value' => $config->get('icontact_api_url'),
            '#description' => t('The API URL without the Folder ID https://app.icontact.com/icp/a/.')
        );
        
        $form['icontact_api_version'] = array(
            '#type' => 'textfield',
            '#title' => t('Api Version'),
            '#required' => TRUE,
            '#default_value' => $config->get('icontact_api_version'),
            '#description' => t('The API Version 2.0 , 2.2 or 2.3 .')
        );
        
        $form['icontact_api_app_id'] = array(
            '#type' => 'textfield',
            '#title' => t('Api App ID'),
            '#required' => TRUE,
            '#default_value' => $config->get('icontact_api_app_id'),
            '#description' => t('The API Application ID')
        );
        
        $form['icontact_api_username'] = array(
            '#type' => 'textfield',
            '#title' => t('Api Username'),
            '#required' => TRUE,
            '#default_value' => $config->get('icontact_api_username')
        );
        
        $form['icontact_api_password'] = array(
            '#type' => 'textfield',
            '#title' => t('Api Password'),
            '#required' => TRUE,
            '#default_value' => $config->get('icontact_api_password')
        );
        
        $form['icontact_api_account_id'] = array(
            '#type' => 'textfield',
            '#title' => t('Account ID'),
            '#required' => TRUE,
            '#default_value' => $config->get('icontact_api_account_id')
        );
        
        return parent::buildForm($form, $form_state);
    }

    /**
     *
     * {@inheritdoc}
     */
    /*
     * public function validateForm(array &$form, FormStateInterface $form_state) {
     * parent::validateForm($form, $form_state);
     * }
     */
    
    /**
     *
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $config = $this->config('webform_icontact.settings');
        $config->set('icontact_api_url', $form_state->getValue('icontact_api_url'))
            ->set('icontact_api_app_id', $form_state->getValue('icontact_api_app_id'))
            ->set('icontact_api_username', $form_state->getValue('icontact_api_username'))
            ->set('icontact_api_password', $form_state->getValue('icontact_api_password'))
            ->set('icontact_api_account_id', $form_state->getValue('icontact_api_account_id'))
            ->set('icontact_api_version', $form_state->getValue('icontact_api_version'))
            ->save();
        
        parent::submitForm($form, $form_state);
    }
}
