<?php
namespace Drupal\webform_icontact\Rest;

interface RestClientInterface
{
    /**
     *
     * @param string $method
     *   get, post, patch, delete, etc. See Guzzle documentation.
     * @param string $endpoint
     *   The iContact API endpoint (ex. people/v2/people)
     * @param array $query
     *   Query string parameters the endpoint allows (ex. ['per_page' => 50]
     * @param array $json 
     *   Utilized for some endpoints
     * @return object
     *   \GuzzleHttp\Psr7\Response body
     */
    public function connect($method, $endpoint, $query, $body);
}

