<?php
namespace Drupal\webform_icontact\Rest;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Messenger\MessengerInterface;
use \GuzzleHttp\ClientInterface;
use \GuzzleHttp\Exception\RequestException;

class RestClient implements RestClientInterface
{

    /**
     * An http client.
     *
     * @var \GuzzleHttp\ClientInterface
     */
    protected $httpClient;

    /**
     * A configuration instance.
     *
     * @var \Drupal\Core\Config\ConfigInterface;
     */
    
    /**
     * Include the messenger service.
     *
     * @var \Drupal\Core\Messenger\MessengerInterface
     */
    protected $messenger;

    protected $config;

    protected $apiVersion;

    protected $apiAppId;

    protected $apiUsername;

    protected $apiPassword;

    protected $apiBaseUri;

    protected $apiAccountId;

    public function __construct(ClientInterface $http_client, ConfigFactory $config_factory, MessengerInterface $messenger)
    {
        $this->httpClient = $http_client;
        $config = $config_factory->get('webform_icontact.settings');
        $this->messenger = $messenger;
        $this->apiVersion = $config->get('icontact_api_version');
        $this->apiAppId = $config->get('icontact_api_app_id');
        $this->apiUsername = $config->get('icontact_api_username');
        $this->apiPassword = $config->get('icontact_api_password');
        $this->apiBaseUri = $config->get('icontact_api_url') . $config->get('icontact_api_account_id') . '/c/';
        $this->apiAccountId = $config->get('icontact_api_account_id');
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Drupal\webform_icontact\Rest\RestClientInterface::connect()
     *
     */
    public function connect($method, $endpoint, $query, $json)
    {
        try {
            $response = $this->httpClient->{$method}($this->apiBaseUri . $endpoint, $this->buildOptions($query, $json));
        } catch (RequestException $exception) {
            $this->messenger->addMessage(t('Failed to connect to iContact API "%error"', [
                '%error' => $exception->getMessage()
            ]), 'error');
            
            \Drupal::logger('webform_icontact')->error('Failed to to connect to iContact API "%error"', [
                '%error' => $exception->getMessage()
            ]);
            return FALSE;
        }
        
        return $response->getBody()->getContents();
    }

    /**
     * Build options for the client.
     */
    private function buildOptions($query, $json)
    {
        $options = [];
        $options['headers'] = $this->auth();
        if ($json) {
            $options['json'] = $json;
        }
        if ($query) {
            $options['query'] = $query;
        }
        return $options;
    }

    /**
     * Handle authentication.
     */
    private function auth()
    {
        return [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Api-version' => $this->apiVersion,
            'Api-AppID' => $this->apiAppId,
            'Api-Username' => $this->apiUsername,
            'Api-Password' => $this->apiPassword
        ];
    }
}

